import React, {Component} from "react"

export default class extends Component {
  render() {
    return (
        <>
        <h1
          style={{
            marginTop: "12.0pt",
            marginRight: "0cm",
            marginBottom: "0cm",
            marginLeft: "0cm",
            lineHeight: "107%",
            fontSize: 21,
            fontFamily: '"Calibri Light",sans-serif',
            color: "#2F5496",
            fontWeight: "normal",
            textAlign: "center"
          }}
        >
          <strong>
            <span style={{ fontFamily: '"Arial",sans-serif', color: "windowtext" }}>
              TEST ZIPPY FRONTEND DEVELOPER
            </span>
          </strong>
        </h1>
        <p
          style={{
            marginTop: "0cm",
            marginRight: "0cm",
            marginBottom: "8.0pt",
            marginLeft: "0cm",
            lineHeight: "107%",
            fontSize: 15,
            fontFamily: '"Calibri",sans-serif'
          }}
        >
          <span
            style={{
              fontSize: 16,
              lineHeight: "107%",
              fontFamily: '"Arial",sans-serif'
            }}
          >
            &nbsp;
          </span>
        </p>
        <p
          style={{
            marginTop: "0cm",
            marginRight: "0cm",
            marginBottom: "8.0pt",
            marginLeft: "0cm",
            lineHeight: "107%",
            fontSize: 15,
            fontFamily: '"Calibri",sans-serif',
            textAlign: "justify"
          }}
        >
          <span
            style={{
              fontSize: 16,
              lineHeight: "107%",
              fontFamily: '"Arial",sans-serif'
            }}
          >
            Se necesita implementar una pequeña app que tenga 1 página principal y dos
            secundarias, las que puedan leer un api y desplegar la información por
            pantalla. para esto debes ocupar todo lo que sabes de React ,Next, Redux
            etc.
          </span>
        </p>
        <p
          style={{
            marginTop: "0cm",
            marginRight: "0cm",
            marginBottom: "8.0pt",
            marginLeft: "0cm",
            lineHeight: "107%",
            fontSize: 15,
            fontFamily: '"Calibri",sans-serif',
            textAlign: "justify"
          }}
        >
          <span
            style={{
              fontSize: 16,
              lineHeight: "107%",
              fontFamily: '"Arial",sans-serif'
            }}
          >
            &nbsp;
          </span>
        </p>
        <p
          style={{
            marginTop: "0cm",
            marginRight: "0cm",
            marginBottom: "8.0pt",
            marginLeft: "0cm",
            lineHeight: "107%",
            fontSize: 15,
            fontFamily: '"Calibri",sans-serif',
            textAlign: "justify"
          }}
        >
          <span
            style={{
              fontSize: 16,
              lineHeight: "107%",
              fontFamily: '"Arial",sans-serif'
            }}
          >
            1-. Descarga el proyecto desde:{" "}
            <strong>
              <a href="https://gitlab.com/jcaroca/testzippyFrontEnd">
                https://gitlab.com/jcaroca/testzippyFrontEnd
              </a>
              ; &nbsp;
            </strong>
            y,<strong>&nbsp;</strong>crea una rama, con el siguiente formato:&nbsp;
          </span>
        </p>
        <p
          style={{
            marginTop: "0cm",
            marginRight: "0cm",
            marginBottom: "8.0pt",
            marginLeft: "0cm",
            lineHeight: "107%",
            fontSize: 15,
            fontFamily: '"Calibri",sans-serif',
            textAlign: "justify"
          }}
        >
          <strong>
            <span
              style={{
                fontSize: 16,
                lineHeight: "107%",
                fontFamily: '"Arial",sans-serif'
              }}
            >
              PrimerNombre_primerapellido_postulante
            </span>
          </strong>
        </p>
        <p
          style={{
            marginTop: "0cm",
            marginRight: "0cm",
            marginBottom: "8.0pt",
            marginLeft: "0cm",
            lineHeight: "107%",
            fontSize: 15,
            fontFamily: '"Calibri",sans-serif',
            textAlign: "justify"
          }}
        >
          <span
            style={{
              fontSize: 16,
              lineHeight: "107%",
              fontFamily: '"Arial",sans-serif'
            }}
          >
            2.- Ejecuta el proyecto con npm &nbsp;:
          </span>
        </p>
        <p
          style={{
            marginTop: "0cm",
            marginRight: "0cm",
            marginBottom: "8.0pt",
            marginLeft: "0cm",
            lineHeight: "107%",
            fontSize: 15,
            fontFamily: '"Calibri",sans-serif',
            textAlign: "justify"
          }}
        >
          <span
            style={{
              fontSize: 16,
              lineHeight: "107%",
              fontFamily: '"Arial",sans-serif'
            }}
          >
            3.-Para el desarrollo del ejercicio utiliza la api:&nbsp;
          </span>
        </p>
        <p
          style={{
            marginTop: "0cm",
            marginRight: "0cm",
            marginBottom: "8.0pt",
            marginLeft: "0cm",
            lineHeight: "107%",
            fontSize: 15,
            fontFamily: '"Calibri",sans-serif',
            textAlign: "justify"
          }}
        >
          <span
            style={{
              fontSize: 16,
              lineHeight: "107%",
              fontFamily: '"Arial",sans-serif'
            }}
          >
            <a href="https://swapi.dev/">https://swapi.dev/</a>
          </span>
        </p>
        <p
          style={{
            marginTop: "0cm",
            marginRight: "0cm",
            marginBottom: "8.0pt",
            marginLeft: "0cm",
            lineHeight: "107%",
            fontSize: 15,
            fontFamily: '"Calibri",sans-serif',
            textAlign: "justify"
          }}
        >
          <span
            style={{
              fontSize: 16,
              lineHeight: "107%",
              fontFamily: '"Arial",sans-serif'
            }}
          >
            <a href="https://swapi.dev/documentation">
              https://swapi.dev/documentation
            </a>
          </span>
        </p>
        <p
          style={{
            marginTop: "0cm",
            marginRight: "0cm",
            marginBottom: "8.0pt",
            marginLeft: "0cm",
            lineHeight: "107%",
            fontSize: 15,
            fontFamily: '"Calibri",sans-serif',
            textAlign: "justify"
          }}
        >
          <span
            style={{
              fontSize: 16,
              lineHeight: "107%",
              fontFamily: '"Arial",sans-serif'
            }}
          >
            4.- Ocupar{" "}
            <span style={{ color: "#202124", background: "white" }}>
              biblioteca&nbsp;
            </span>
            de diseño (
            <strong>
              <span style={{ color: "#202124", background: "white" }}>
                Material UI o la que conozcas)&nbsp;
              </span>
            </strong>
            <span style={{ color: "#202124", background: "white" }}>
              con diseño<strong>&nbsp;</strong>responsivo.
            </span>
          </span>
        </p>
        <p
          style={{
            marginTop: "0cm",
            marginRight: "0cm",
            marginBottom: "8.0pt",
            marginLeft: "0cm",
            lineHeight: "107%",
            fontSize: 15,
            fontFamily: '"Calibri",sans-serif',
            textAlign: "justify"
          }}
        >
          <span
            style={{
              fontSize: 16,
              lineHeight: "107%",
              fontFamily: '"Arial",sans-serif'
            }}
          >
            5.- Cargar todos los films en una tabla de front &nbsp; pagina 1, de la
            siguiente manera:
          </span>
        </p>
        <div
          style={{
            marginTop: "0cm",
            marginRight: "0cm",
            marginBottom: "8.0pt",
            marginLeft: "18.0pt",
            lineHeight: "107%",
            fontSize: 15,
            fontFamily: '"Calibri",sans-serif',
            border: "solid #CCCCCC 1.0pt",
            padding: "7.0pt 7.0pt 7.0pt 7.0pt",
            background: "whitesmoke"
          }}
        >
          <ul style={{ listStyleType: "disc", marginLeft: 8 }}>
            <li>
              <span
                style={{
                  fontFamily: '"Arial",sans-serif',
                  fontSize: "12.0pt",
                  color: "black"
                }}
              >
                title: pintar titulo
              </span>
            </li>
            <li>
              <span
                style={{
                  fontFamily: '"Arial",sans-serif',
                  fontSize: "12.0pt",
                  color: "black"
                }}
              >
                episode_id: pintar episode_id
              </span>
            </li>
            <li>
              <span
                style={{
                  fontFamily: '"Arial",sans-serif',
                  fontSize: "12.0pt",
                  color: "black"
                }}
              >
                opening_crawl: pintar opening_crawl
              </span>
            </li>
            <li>
              <span
                style={{
                  fontFamily: '"Arial",sans-serif',
                  fontSize: "12.0pt",
                  color: "black"
                }}
              >
                director: pintar director
              </span>
            </li>
            <li>
              <span
                style={{
                  fontFamily: '"Arial",sans-serif',
                  fontSize: "12.0pt",
                  color: "black"
                }}
              >
                producer: pintar producer
              </span>
            </li>
            <li>
              <span style={{ fontFamily: '"Arial",sans-serif' }}>release_date:</span>
              <span
                style={{
                  fontFamily: '"Arial",sans-serif',
                  color: "black",
                  fontSize: 16
                }}
              >
                &nbsp;pintar
              </span>
              <span style={{ fontFamily: '"Arial",sans-serif' }}>
                &nbsp;release_date
              </span>
            </li>
            <li>
              <span style={{ fontFamily: '"Arial",sans-serif' }}>
                characters: cantidad total,
              </span>
            </li>
            <li>
              <span style={{ fontFamily: '"Arial",sans-serif' }}>
                planets: cantidad total,
              </span>
            </li>
            <li>
              <span style={{ fontFamily: '"Arial",sans-serif' }}>
                starships:cantidad total,
              </span>
            </li>
            <li>
              <span style={{ fontFamily: '"Arial",sans-serif' }}>
                vehicles: cantidad total,
              </span>
            </li>
            <li>
              <span style={{ fontFamily: '"Arial",sans-serif' }}>
                species:cantidad total,
              </span>
            </li>
            <li>
              <span style={{ fontFamily: '"Arial",sans-serif' }}>
                BOTON GO TO: characters,
              </span>
            </li>
            <li>
              <span style={{ fontFamily: '"Arial",sans-serif' }}>
                BOTON GO TO: planets
              </span>
            </li>
          </ul>
        </div>
        <p
          style={{
            marginTop: "0cm",
            marginRight: "0cm",
            marginBottom: "8.0pt",
            marginLeft: "0cm",
            lineHeight: "107%",
            fontSize: 15,
            fontFamily: '"Calibri",sans-serif'
          }}
        >
          <span
            style={{
              fontSize: 16,
              lineHeight: "107%",
              fontFamily: '"Arial",sans-serif'
            }}
          >
            &nbsp;
          </span>
        </p>
        <p
          style={{
            marginTop: "0cm",
            marginRight: "0cm",
            marginBottom: "8.0pt",
            marginLeft: "0cm",
            lineHeight: "107%",
            fontSize: 15,
            fontFamily: '"Calibri",sans-serif'
          }}
        >
          <span
            style={{
              fontSize: 16,
              lineHeight: "107%",
              fontFamily: '"Arial",sans-serif'
            }}
          >
            6.- Búsqueda de elementos cargados en la tabla por los siguientes
            parámetros:
          </span>
        </p>
        <div
          style={{
            marginTop: "0cm",
            marginRight: "0cm",
            marginBottom: "8.0pt",
            marginLeft: "18.0pt",
            lineHeight: "107%",
            fontSize: 15,
            fontFamily: '"Calibri",sans-serif',
            border: "solid #CCCCCC 1.0pt",
            padding: "7.0pt 7.0pt 7.0pt 7.0pt",
            background: "whitesmoke"
          }}
        >
          <ul style={{ listStyleType: "disc", marginLeft: 8 }}>
            <li>
              <span
                style={{
                  fontFamily: '"Arial",sans-serif',
                  fontSize: "12.0pt",
                  color: "#3A3F44"
                }}
              >
                title
              </span>
            </li>
            <li>
              <span
                style={{
                  fontFamily: '"Arial",sans-serif',
                  color: "#3A3F44",
                  fontSize: 16
                }}
              >
                episode_id
              </span>
            </li>
            <li>
              <span
                style={{
                  fontFamily: '"Arial",sans-serif',
                  color: "#3A3F44",
                  fontSize: 16
                }}
              >
                opening_crawl
              </span>
            </li>
            <li>
              <span
                style={{
                  fontFamily: '"Arial",sans-serif',
                  color: "#3A3F44",
                  fontSize: 16
                }}
              >
                director
              </span>
            </li>
            <li>
              <span
                style={{
                  fontFamily: '"Arial",sans-serif',
                  color: "#3A3F44",
                  fontSize: 16
                }}
              >
                producer
              </span>
            </li>
            <li>
              <span
                style={{
                  fontFamily: '"Arial",sans-serif',
                  color: "#3A3F44",
                  fontSize: 16
                }}
              >
                release_date
              </span>
            </li>
          </ul>
        </div>
        <p
          style={{
            marginTop: "0cm",
            marginRight: "0cm",
            marginBottom: "8.0pt",
            marginLeft: "0cm",
            lineHeight: "107%",
            fontSize: 15,
            fontFamily: '"Calibri",sans-serif'
          }}
        >
          <span
            style={{
              fontSize: 16,
              lineHeight: "107%",
              fontFamily: '"Arial",sans-serif'
            }}
          >
            &nbsp;
          </span>
        </p>
        <p
          style={{
            marginTop: "0cm",
            marginRight: "0cm",
            marginBottom: "8.0pt",
            marginLeft: "0cm",
            lineHeight: "107%",
            fontSize: 15,
            fontFamily: '"Calibri",sans-serif'
          }}
        >
          <span
            style={{
              fontSize: 16,
              lineHeight: "107%",
              fontFamily: '"Arial",sans-serif'
            }}
          >
            7.-TABLA characters en &nbsp;página 2
          </span>
        </p>
        <p
          style={{
            marginTop: "0cm",
            marginRight: "0cm",
            marginBottom: "8.0pt",
            marginLeft: "0cm",
            lineHeight: "107%",
            fontSize: 15,
            fontFamily: '"Calibri",sans-serif'
          }}
        >
          <span
            style={{
              fontSize: 16,
              lineHeight: "107%",
              fontFamily: '"Arial",sans-serif'
            }}
          >
            Hacer click en el boton &nbsp; “BOTON GO TO: characters” de la tabla
            ubicada en la página 1 &nbsp; y mostrar otra tabla con los
            &nbsp;characters del films en página 2
          </span>
        </p>
        <p
          style={{
            marginTop: "0cm",
            marginRight: "0cm",
            marginBottom: "8.0pt",
            marginLeft: "0cm",
            lineHeight: "107%",
            fontSize: 15,
            fontFamily: '"Calibri",sans-serif'
          }}
        >
          <span
            style={{
              fontSize: 16,
              lineHeight: "107%",
              fontFamily: '"Arial",sans-serif'
            }}
          >
            &nbsp;
          </span>
        </p>
        <p
          style={{
            marginTop: "0cm",
            marginRight: "0cm",
            marginBottom: "8.0pt",
            marginLeft: "0cm",
            lineHeight: "107%",
            fontSize: 15,
            fontFamily: '"Calibri",sans-serif'
          }}
        >
          <span
            style={{
              fontSize: 16,
              lineHeight: "107%",
              fontFamily: '"Arial",sans-serif'
            }}
          >
            8.-TABLA planets en página 3
          </span>
        </p>
        <p
          style={{
            marginTop: "0cm",
            marginRight: "0cm",
            marginBottom: "8.0pt",
            marginLeft: "0cm",
            lineHeight: "107%",
            fontSize: 15,
            fontFamily: '"Calibri",sans-serif'
          }}
        >
          <span
            style={{
              fontSize: 16,
              lineHeight: "107%",
              fontFamily: '"Arial",sans-serif'
            }}
          >
            Hacer click en el boton &nbsp; “BOTON GO TO: planets” de la tabla ubicada
            en la página 1 &nbsp;y mostrar otra tabla con los &nbsp;planets del films
            en página 3
          </span>
        </p>
        <p
          style={{
            marginTop: "0cm",
            marginRight: "0cm",
            marginBottom: "8.0pt",
            marginLeft: "0cm",
            lineHeight: "107%",
            fontSize: 15,
            fontFamily: '"Calibri",sans-serif'
          }}
        >
          <span
            style={{
              fontSize: 16,
              lineHeight: "107%",
              fontFamily: '"Arial",sans-serif'
            }}
          >
            &nbsp;
          </span>
        </p>
        <p
          style={{
            marginTop: "0cm",
            marginRight: "0cm",
            marginBottom: "8.0pt",
            marginLeft: "0cm",
            lineHeight: "107%",
            fontSize: 15,
            fontFamily: '"Calibri",sans-serif'
          }}
        >
          <span
            style={{
              fontSize: 16,
              lineHeight: "107%",
              fontFamily: '"Arial",sans-serif'
            }}
          >
            9.-Crear un widget o un elemento visual que se ubica &nbsp; arriba de la
            tabla principal contenga los siguientes datos:
          </span>
        </p>
        <ul style={{ listStyleType: "disc" }}>
          <li>
            <span
              style={{
                lineHeight: "107%",
                fontFamily: '"Arial",sans-serif',
                fontSize: 16
              }}
            >
              Widget_1: cantidad total de{" "}
              <span style={{ color: "#3A3F44" }}>characters</span> distintos de todos
              los Films.
            </span>
          </li>
          <li>
            <span
              style={{
                lineHeight: "107%",
                fontFamily: '"Arial",sans-serif',
                fontSize: 16
              }}
            >
              Widget_2: cantidad total de{" "}
              <span style={{ color: "#3A3F44" }}>planets&nbsp;</span>distintos de
              todos los Films.
            </span>
          </li>
          <li>
            <span
              style={{
                lineHeight: "107%",
                fontFamily: '"Arial",sans-serif',
                fontSize: 16
              }}
            >
              Widget_3: cantidad total de{" "}
              <span style={{ color: "#3A3F44" }}>vehicles&nbsp;</span>distintos de
              todos los Films.
            </span>
          </li>
        </ul>
        <p
          style={{
            marginTop: "0cm",
            marginRight: "0cm",
            marginBottom: "8.0pt",
            marginLeft: "0cm",
            lineHeight: "107%",
            fontSize: 15,
            fontFamily: '"Calibri",sans-serif'
          }}
        >
          <span
            style={{
              fontSize: 16,
              lineHeight: "107%",
              fontFamily: '"Arial",sans-serif'
            }}
          >
            10.- No temas en aportar valor, deja que tu creatividad fluya, cualquier
            aporte será bien recibido.
          </span>
        </p>
        <p
          style={{
            marginTop: "0cm",
            marginRight: "0cm",
            marginBottom: "8.0pt",
            marginLeft: "0cm",
            lineHeight: "107%",
            fontSize: 15,
            fontFamily: '"Calibri",sans-serif'
          }}
        >
          <span
            style={{
              fontSize: 16,
              lineHeight: "107%",
              fontFamily: '"Arial",sans-serif'
            }}
          >
            &nbsp;
          </span>
        </p>
        <p
          style={{
            marginTop: "0cm",
            marginRight: "0cm",
            marginBottom: "8.0pt",
            marginLeft: "0cm",
            lineHeight: "107%",
            fontSize: 15,
            fontFamily: '"Calibri",sans-serif'
          }}
        >
          <span
            style={{
              fontSize: 16,
              lineHeight: "107%",
              fontFamily: '"Arial",sans-serif'
            }}
          >
            11. Cuando termines sube tu rama, al repositorio.
          </span>
        </p>
      </>
      
    )
  }
}